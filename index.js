const stripeHandler = require('./code/stripe.handle')
let stripe;
/**
 * @constructor
 */
module.exports.config = (secretKey = 'sk_test_8QmSxCZeMGJNRa1bVnUURpkd0014Sc9kE5', publishableKey = 'pk_test_W4broeCQp1crkflQwbaBhieL00xyTlHun6', setApiVersion = '2018-02-28') => {
    stripe = require('stripe')(secretKey)
    stripe.setApiVersion(setApiVersion);
    stripe = stripeHandler(stripe);
    return stripe
}
/**
 * @async 
 * @param {*}source
 * @param {*}email
 * @param {*}description
 * @returns {object} customer
 */
module.exports.createCustomer = (source, email, description) => {
    return stripe.createCustomer(source, email, description)
}
/**
 * @async 
 * @param {number}amount
 * @param {string}currency
 * @param {string}source
 * @param {string}description
 * @returns {object} charge
 */
module.exports.createCharge = (amount, currency, source, description) => {
    return stripe.createCharge(amount, currency, source, description)
}
/**
 * @async 
 * @param {string}customer
 * @param {Array}items
 * @returns {object} subscribe
 */
module.exports.creatSubscribe = (customer, items) => {
    return stripe.creatSubscribe(customer, items)
}