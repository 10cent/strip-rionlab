const config = require('../config/config')

class ConfigCommon {
    getStripe() {
        return config.stripe
    }
}
module.exports = new ConfigCommon();
