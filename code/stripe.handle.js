
'use strict';
class Stripe {
  constructor(stripe) {
    this.stripe = stripe;
  }
  async  createCustomer(source, email, description = 'Customer for jenny.rosen@example.com') {
    let customer = await this.stripe.customers.create({
      description: description,
      source: source // obtained with Stripe.js
      , email: email
    });
    return customer
  }
  async createCharge(amount = 2000, currency = "usd", source = "tok_mastercard", description = "Charge for jenny.rosen@example.com") {
    let charge = await this.stripe.charges.create({
      amount: amount,
      currency: currency,
      source: source, // obtained with Stripe.js
      description: description
    }, function (err, charge) {
      console.log(err);
    });
    return charge;
  }

  async creatSubscribe(customer = 'cus_4fdAW5ftNQow1a', items = [{ plan: 'plan_CBXbz9i7AIOTzr' }]) {
    const subscription = await stripe.subscriptions.create({
      customer: customer,
      items: items,
    })
    return subscription
  }
  async  createPlans(interval, product, nickname, amount) {
    const plan = await stripe.plans.create({
      currency: currency,
      interval: interval,
      product: product,
      nickname: nickname,
      amount: amount,
    })
    return plan
  }
  async payment(numbercard = '4242424242424242', expMonth = 12, expYear = 2020, cvc = '123') {
    let payment = await this.stripe.paymentMethods.create({
      type: "card",
      card: {
        number: numbercard,
        exp_month: expMonth,
        exp_year: expYear,
        cvc: cvc
      }
    }, function (err, token) {
      console.log(err);
    });
  }
}
module.exports = (secretKey, publishableKey) => { return new Stripe(secretKey, publishableKey) }